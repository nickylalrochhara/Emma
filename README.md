# Miss Emma

To anyone coming here to **clone this bot**, no support will be given for it. If you dare to join the support group and asks for how to run this bot, you'll be banned with no warning and you can blame no one but yourself for that.

A modular Telegram Python bot running on python3 with an sqlalchemy database.

Can be found on telegram as [Miss Emma](https://t.me/dEmma_Bot).

Alternatively, [find me on telegram](https://t.me/NickyLrca)! (Keep all support questions in the support chat, where more people can help you.)

You can also join our support group [here!](https://t.me/dEmma_Bot)
(Keep in the mind that we does not support any fork of Miss Emma.)

Help us bring more languages to the bot by contributing to the project in [Crowdin](https://crowdin.com/project/Emma)!

## Contributing to the project
* You must sign off on your commit.
* You must sign the commit via GPG Key.
* Make sure your PR passes all CI.

## Thanks to
* NickyLrca - Current Miss Emma Owner
* zakaryan2004 - For helping out a lot with this project.
* MrYacha - For Yana :3
* Skittle - For memes and sticker stuff.
* 1mavarick1 - Introducing Global Mutes, etc.
* FamhawiteInfosysReal - Reworked Welcome, Fed v2
* Paul Larsen - Marie and Rose creator, inspired me to do many things.

And much more that we couldn't list it here!
