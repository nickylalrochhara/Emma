FROM registry.gitlab.com/nickylalrochhara/emma:dockerstation-beta

RUN git clone https://gitlab.com/nickylalrochhara/emma.git -b main /data/emma

COPY ./config.yml /data/emma

WORKDIR /data/emma

CMD ["python", "-m", "emma"]
